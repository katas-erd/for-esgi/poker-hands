package poker;


/*
Mdélise une carte :
 - une couleur (trèfle, pique, coeur, carreau)
 - une valeur (2 à 10, J, Q, K, A)

 Une carte peut être comparée à une autre (méthode compareTo). Seule sa valeur permet de la classer (un 3 est plus petit qu'un valet peu importe la couleur)
 Par contre, l'égalité stricte se base sur la valeur et la couleur (Deux cartes reine de coeur sont identiques. Mais la reine de coeur et la reine de pique sont différentes)
 */
public record Card(CardSuit suit, CardValue value) implements Comparable<Card> {

    @Override
    public int compareTo(Card other) {
        return value.compareTo(other.value);
    }

    @Override
    public String toString() {
        return suit.symbol + value.representation;
    }

}
