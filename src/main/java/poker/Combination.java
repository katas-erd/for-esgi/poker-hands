package poker;

import org.assertj.core.util.Sets;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;


/*
Modélise une combinaison possible avec les cartes données

Une combinaison comprend une règle et des cartes regroupés ensemble.
Par exemple, dans un carré "♢K ♤K ♡K ♧K ♢6", on a la règle FourOfAKind et les groupes 4 x rois et 1 x 6
Les groupes permettent de comparer les combinaison de même type (par exemple comparer deux carrés ou deux flush)
C'est important de garder l'ordre des groupes.

Par exemple, dans le cas d'une pair "♢K ♤K ♡4 ♧5 ♢6" contre une pair "♢T ♤T ♡8 ♧5 ♢6", on a :
- la combinaison Pair + 2 x K, 1 x 6, 1 x 5, 1 x 4
- la combinaison Pair + 2 x T, 1 x 8, 1 x 6, 1 x 5

Vu qu'on trouve la même règle (Pair), on va comparer les valeurs d'abord des pairs elles-mêmes (K vs T)

Pour le cas de "♢K ♤K ♡4 ♧5 ♢6" contre "♡K ♧K ♢4 ♤T ♢6", on a :
- la combinaison Pair + 2 x K, 1 x 6, 1 x 5, 1 x 4
- la combinaison Pair + 2 x K, 1 x T, 1 x 6, 1 x 4

On va comparer les Pairs. Comme elles sont identiques (K vs K), on regarde la carte la plus forte suivante (6 vs T).
La seconde Pair gagne
 */
public class Combination implements Comparable<Combination> {

    CombinationType type;
    List<OfAKind> ofAKinds;

    public Combination(CombinationType type, List<OfAKind> ofAKinds) {
        this.type = type;
        this.ofAKinds = ofAKinds;

        ofAKinds.sort(Comparator.comparing(OfAKind::getCount).thenComparing(OfAKind::getCard).reversed());
    }

    Combination(CombinationType type, OfAKind ofAKind) {
        this(type, Arrays.asList(ofAKind));
    }
    int weight() {
        return type.weight;
    }

    @Override
    public int compareTo(Combination o) {
        int c = this.weight() - o.weight();
        if (c != 0)
            return c;

        for (int i = 0; i != ofAKinds.size(); ++i) {
            Card left = ofAKinds.get(i).getCard();
            Card right = o.ofAKinds.get(i).getCard();
            int cardCompare = left.compareTo(right);
            if (cardCompare != 0)
                return cardCompare;
        }
        return 0;
    }

    @Override
    public String toString() {
        return type + ": " + ofAKinds;
    }
}
