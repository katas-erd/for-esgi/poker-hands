package poker;

import java.util.Comparator;
import java.util.List;

public class Hand implements Comparable<Hand> {
    List<Card> cards;

    Combination combination;

    public Hand(List<Card> cards) {
        assert cards.size() == 5;

        this.cards = cards;
        cards.sort(Comparator.reverseOrder());
        this.combination = new Rules().findCombination(cards);
    }

    public Combination bestCombination() {
        return combination;
    }

    @Override
    public int compareTo(Hand o) {
        return this.combination.compareTo(o.combination);
    }

    @Override
    public String toString() {
        return "Hand{" +
                "cards=" + cards +
                ", combination=" + combination +
                '}';
    }
}
