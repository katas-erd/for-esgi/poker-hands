package poker;

/*
Représente la couleur d'une carte (Suit en anglais)
 */
public enum CardSuit {
    HEART("♡"),
    DIAMOND("♢"),
    CLUBS("♧"),
    SPADE("♤");

    String symbol;

    CardSuit(String symbol) {
        this.symbol = symbol;
    }
}