package poker;

import java.util.Objects;

public class OfAKind {

    Card card;
    int count;

    public OfAKind(Card card, int count) {
        this.card = card;
        this.count = count;
    }

    public Card getCard() {
        return card;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return card.value().representation +
                " x " + count;
    }
}
