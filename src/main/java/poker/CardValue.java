package poker;

/*
Les enums peuvent parfois ne pas être la meilleure approche pour modéliser un type énuméré.
Par exemple, ici, on aimerait bien pouvoir comparer deux poker.CardValue à partir de leur poids (leur ordre dans le jeu).
Sauf qu'on ne peut pas définir la méthode compareTo (enum implémente déjà l'interface Comparable)

Du coup, on va utiliser une autre approche : une classe et des constantes.
Pour empêcher de pouvoir créer de nouvelles cartes, on va rendre le constructeur privé (on abordera ce point durant le prochain cours)
Ce qui fait qu'il ne sera plus possbile de créer de nouveau objet de la classe poker.CardValue hormis ceux définis par la classe elle-même.

 */
public class CardValue implements Comparable<CardValue> {

    /*
    On crée un objet par carte
     */
    public final static CardValue TWO = new CardValue("2", 1);
    public final static CardValue THREE = new CardValue("3", 2);
    public final static CardValue FOUR = new CardValue("4", 3);
    public final static CardValue FIVE = new CardValue("5", 4);
    public final static CardValue SIX = new CardValue("6", 5);
    public final static CardValue SEVEN = new CardValue("7", 6);
    public final static CardValue EIGHT = new CardValue("8", 7);
    public final static CardValue NINE = new CardValue("9", 8);
    public final static CardValue TEN = new CardValue("T", 9);
    public final static CardValue JACK = new CardValue("J", 10);
    public final static CardValue QUEEN = new CardValue("Q", 11);
    public final static CardValue KING = new CardValue("K", 12);
    public final static CardValue ACE = new CardValue("A", 13);

    String representation;
    int weight;

    private CardValue(String representation, int weight) {
        this.representation = representation;
        this.weight = weight;
    }

    @Override
    public int compareTo(CardValue other) {
        return weight - other.weight;
    }
}
