package poker;

import java.util.*;

public class Rules {
    public Combination findCombination(List<Card> cards) {
        List<Combination> allCombinations = Arrays.asList(
                straightFlush(cards),
                fourOfAKind(cards),
                fullHouse(cards),
                flush(cards),
                straight(cards),
                threeOfAKind(cards),
                twoPairs(cards),
                pair(cards)
        );

        for (Combination combination: allCombinations) {
            if (combination != null)
                return combination;
        }

        return highCard(cards);
    }

    public Combination straightFlush(List<Card>cards) {
        Set<CardSuit> suits = new HashSet<>();
        for(Card card: cards) {
            suits.add(card.suit());
        }

        if (suits.size() == 1 && cards.get(0).value().weight - cards.get(4).value().weight == 4)
            return new Combination(CombinationType.StraightFlush, new OfAKind(cards.get(0), 1));
        return null;
    }

    public Combination fourOfAKind(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean containsFourOfAKind = cardsGroupedByValue.numberOfGroupsOfAtLeast(4) != 0;
        if (containsFourOfAKind)
            return new Combination(CombinationType.FourOfAKind, cardsGroupedByValue.all());
        return null;
    }

    public Combination fullHouse(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean containsThreeOfAKind = cardsGroupedByValue.numberOfGroupsOf(3) != 0;
        boolean containsPair = cardsGroupedByValue.numberOfGroupsOf(2) != 0;
        if (containsThreeOfAKind && containsPair)
            return new Combination(CombinationType.FullHouse, cardsGroupedByValue.all());
        return null;
    }

    public Combination flush(List<Card> cards) {
        Set<CardSuit> suits = new HashSet<>();
        for(Card card: cards) {
            suits.add(card.suit());
        }
        if (suits.size() == 1)
            return new Combination(CombinationType.Flush, groupCardsByTheirValues(cards).all());
        return null;
    }

    public Combination straight(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean allCardsAreUnique = cardsGroupedByValue.all().size() == 5;
        if (allCardsAreUnique
                && cards.get(0).value().weight - cards.get(4).value().weight == 4) // Si la différence entre la carte la plus haute et la carte la plus basse est de 4, ça veut dire qu'on a un exemplaire de chaque carte entre les deux
            return new Combination(CombinationType.Straight, new OfAKind(cards.get(0), 1));
        return null;
    }

    public Combination threeOfAKind(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean containsThreeOfAKind = cardsGroupedByValue.numberOfGroupsOf(3) == 1;
        if (containsThreeOfAKind)
            return new Combination(CombinationType.ThreeOfAKind, cardsGroupedByValue.all());
        return null;
    }

    public Combination twoPairs(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean containsTwoPairs = cardsGroupedByValue.numberOfGroupsOf(2) == 2;
        if (containsTwoPairs)
            return new Combination(CombinationType.TwoPairs, cardsGroupedByValue.all());
        return null;
    }

    public Combination pair(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        boolean containsPairs = cardsGroupedByValue.numberOfGroupsOf(2) == 1;
        if (containsPairs)
            return new Combination(CombinationType.Pair, cardsGroupedByValue.all());
        return null;
    }

    public Combination highCard(List<Card>cards) {
        CardsGroupedByValue cardsGroupedByValue = groupCardsByTheirValues(cards);
        return new Combination(CombinationType.HighCard, cardsGroupedByValue.all());
    }

    CardsGroupedByValue groupCardsByTheirValues(List<Card> cards) {
        CardsGroupedByValue cardsGroupedByValue = new CardsGroupedByValue();
        for(Card card: cards) {
            cardsGroupedByValue.put(card);
        }
        return cardsGroupedByValue;
    }
}

class CardsGroupedByValue {
    Map<Card, Integer> h = new HashMap<>();

    void put(Card card) {
        for(Card k: h.keySet()) {
            if (k.compareTo(card) == 0) { // une carte est unique de par sa valeur et sa couleur. Mais est comparée par rapport à sa valeur (et, on ne s'intéresse qu'à la valeur, ici)
                h.put(k, h.getOrDefault(k, 0) + 1);
                return;
            }
        }
        h.put(card, 1);
    }

    List<OfAKind> groupsOfAtLeast(int size) {
        List<OfAKind> groups = new ArrayList<>();
        for(Map.Entry<Card, Integer> entry: h.entrySet()) {
            if (entry.getValue() >= size)
                groups.add(new OfAKind(entry.getKey(),  entry.getValue()));
        }
        return groups;
    }

    int numberOfGroupsOfAtLeast(int size) {
        return groupsOfAtLeast(size).size();
    }

    List<OfAKind> all() {
        return groupsOfAtLeast(0);
    }

    public int numberOfGroupsOf(int size) {
        List<OfAKind> groups = groupsOfAtLeast(size);
        int count = 0;
        for(OfAKind g: groups) {
            if (g.count == size)
                ++count;
        }
        return count;
    }
}