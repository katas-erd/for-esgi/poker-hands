package poker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static poker.utils.Utils.hand_with;
import static poker.utils.Utils.toParts;

public class RuleTest {
    @Test
    @DisplayName("An hand should contain exactly 5 cards")
    public void test0() {
        try {
            hand_with("♢J ♢K ♤J ♡4 ♧2");
        } catch (AssertionError e) {
            Assertions.fail();
        }

        try {
            hand_with("♢J ♢K ♤J ♡4");
            Assertions.fail("poker.Hand should not contain less than 5 cards");
        } catch (AssertionError e) {
        }

        try {
            hand_with("♢8 ♢J ♢K ♤J ♡4 ♧2");
            Assertions.fail("poker.Hand should not contain more than 5 cards");
        } catch (AssertionError e) {
        }
    }

    @Test
    @DisplayName("Should find the highest card")
    void test1() {
        assertThat(hand_with("♢A ♢J ♤3 ♡2 ♧K").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.HighCard, toParts("♢A", "♧K", "♢J", "♤3", "♡2")));
    }

    @Test
    @DisplayName("Should find a Straight Flush")
    void test2() {
        assertThat(hand_with("♢2 ♢3 ♢4 ♢5 ♢6").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.StraightFlush, toParts("♢6")));
    }

    @Test
    @DisplayName("Should find a four of a kind")
    void test3() {
        assertThat(hand_with("♢K ♤K ♡K ♧K ♢6").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.FourOfAKind, toParts("♢K ♤K ♡K ♧K", "♢6")));
    }

    @Test
    @DisplayName("Should find a full house")
    void test4() {
        assertThat(hand_with("♢2 ♢J ♤2 ♡2 ♧J").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.FullHouse, toParts("♢2 ♤2 ♡2", "♢J ♧J")));
    }

    @Test
    @DisplayName("Should find a flush")
    void test5() {
        assertThat(hand_with("♧2 ♧J ♧3 ♧5 ♧K").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.Flush, toParts("♧2", "♧J", "♧3", "♧5", "♧K")));
    }

    @Test
    @DisplayName("Should find a three of a kind")
    void test6() {
        assertThat(hand_with("♢2 ♢Q ♤2 ♡2 ♧J").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.ThreeOfAKind, toParts("♢2 ♤2 ♡2", "♢Q", "♧J")));
    }

    @Test
    @DisplayName("Should find two pairs")
    void test7() {
        assertThat(hand_with("♢3 ♢J ♤2 ♡2 ♧J").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.TwoPairs, toParts("♢J ♧J", "♤2 ♡2", "♢3")));
    }

    @Test
    @DisplayName("Should find a pair")
    void test8() {
        assertThat(hand_with("♢2 ♢7 ♤5 ♡2 ♧J").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.Pair, toParts("♢2 ♡2", "♤5", "♢7", "♧J")));
    }

    @Test
    @DisplayName("Should find a straight")
    void test9() {
        assertThat(hand_with("♢6 ♢5 ♤2 ♡3 ♧4").bestCombination())
                .isEqualByComparingTo(new Combination(CombinationType.Straight, toParts("♢6")));
    }
}


