package poker;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static poker.utils.Utils.the_hand;

public class HandTest {

    @Test
    @DisplayName("should win by its higher rank")
    void test0() {
        the_hand("♢A ♢J ♤3 ♡2 ♧K").should_win_against("♢5 ♢J ♤3 ♡2 ♧K");
    }

    @Test
    @DisplayName("should win by its second higher rank")
    void test1() {
        the_hand("♢A ♢J ♤3 ♡2 ♧K").should_win_against("♢A ♢J ♤3 ♡2 ♧Q");
    }

    @Test
    @DisplayName("should win against weaker hand")
    void test2() {
        the_hand("♢J ♢K ♤J ♡4 ♧2").should_win_against("♢2 ♧J ♧3 ♡2 ♧Q");
    }

    @Test
    @DisplayName("hand with the highest pair should win")
    void test3() {
        the_hand("♢J ♢K ♤J ♡4 ♧2").should_win_against("♢3 ♧J ♧3 ♡6 ♧Q");
    }

    @Test
    @DisplayName("hand with the highest card should win when pair are the same")
    void test4() {
        the_hand("♢J ♢K ♤J ♡4 ♧2").should_win_against("♢2 ♧J ♧3 ♡J ♧Q");
    }

    @Test
    @DisplayName("should win against weaker hand")
    void test5() {
        the_hand("♢J ♢K ♤J ♡4 ♧2").should_win_against("♢2 ♧J ♧3 ♡2 ♧Q");
    }

    @Test
    @DisplayName("hand with the highest pair should win ")
    void test6() {
        the_hand("♢J ♢4 ♤J ♡4 ♧2").should_win_against("♢3 ♧5 ♧3 ♡5 ♧Q");
    }

    @Test
    @DisplayName("hand with the second highest pair should win ")
    void test7() {
        the_hand("♢4 ♤J ♡4 ♧2 ♢J").should_win_against("♧J ♢3 ♧3 ♡J ♧Q");
    }

    @Test
    @DisplayName("hand with the highest card should win when pairs are the same")
    void test8() {
        the_hand("♢J ♢K ♤J ♡4 ♧2").should_win_against("♢2 ♧J ♧3 ♡J ♧Q");
    }

    @Test
    @DisplayName("should win against weaker hand")
    void test9() {
        the_hand("♢J ♢K ♤J ♡J ♧2").should_win_against("♢3 ♧J ♧3 ♡5 ♧5");
    }

    @Test
    @DisplayName("the highest value should win")
    void test10() {
        the_hand("♢J ♢K ♤J ♡J ♧2").should_win_against("♢3 ♧J ♧3 ♡3 ♧Q");
    }

    @Test
    @DisplayName("A straight should win against weaker hand")
    void test11() {
        the_hand("♢2 ♢3 ♤4 ♡5 ♧6").should_win_against("♢K ♧K ♧3 ♡K ♧9");
    }

    @Test
    @DisplayName("the highest value should win")
    void test12() {
        the_hand("♢T ♢J ♤Q ♡K ♧A").should_win_against("♢7 ♢8 ♤9 ♡T ♧J");
    }

    @Test
    @DisplayName("A flush should win against weaker hand")
    void test13() {
        the_hand("♧2 ♧J ♧3 ♧5 ♧Q").should_win_against("♢2 ♢3 ♤4 ♡5 ♧6");
    }

    @Test
    @DisplayName("A flush should win by its higher card")
    void test14() {
        the_hand("♧2 ♧J ♧3 ♧5 ♧K").should_win_against("♢2 ♢Q ♢3 ♢4 ♢9");
    }

    @Test
    @DisplayName("should be even")
    void test15() {
        the_hand("♧2 ♧J ♧3 ♧5 ♧K").should_be_even("♢2 ♢J ♢3 ♢5 ♢K");
    }

    @Test
    @DisplayName("A flush should win against weaker hand")
    void test16() {
        the_hand("♢2 ♢J ♤2 ♡2 ♧J").should_win_against("♧2 ♧J ♧3 ♧5 ♧Q");
    }

    @Test
    @DisplayName("A flush should win by its higher card")
    void test17() {
        the_hand("♢7 ♢J ♤7 ♡7 ♧J").should_win_against("♢2 ♢J ♤2 ♡2 ♧J");
    }

    @Test
    @DisplayName("should win against weaker hand")
    void test18() {
        the_hand("♢2 ♢J ♤2 ♡2 ♧2").should_win_against("♢2 ♢J ♤2 ♡2 ♧J");
    }

    @Test
    @DisplayName("A flush should win by its higher card")
    void test19() {
        the_hand("♢7 ♢J ♤7 ♡7 ♧7").should_win_against("♢2 ♢J ♤2 ♡2 ♧2");
    }

    @Test
    @DisplayName("should win against weaker hand")
    void test20() {
        the_hand("♢2 ♢3 ♢4 ♢5 ♢6").should_win_against("♢2 ♢J ♤2 ♡2 ♧2");
    }

    @Test
    @DisplayName("A flush should win by its higher rank")
    void test21() {
        the_hand("♢T ♢J ♢Q ♢K ♢A").should_win_against("♢7 ♢8 ♢9 ♢T ♢J");
    }
}
