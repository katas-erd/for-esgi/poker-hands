package poker;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static poker.utils.Utils.toCard;

public class CardTest {
    @Test
    @DisplayName("higher card should beat lower card")
    void test1() {
        leftShouldBeatRight("♡A", "♢K");
        leftShouldBeatRight("♡K", "♢Q");
        leftShouldBeatRight("♡Q", "♢J");
        leftShouldBeatRight("♡J", "♢T");
        leftShouldBeatRight("♡T", "♢9");
        leftShouldBeatRight("♡9", "♢8");
        leftShouldBeatRight("♡8", "♢7");
        leftShouldBeatRight("♡7", "♢6");
        leftShouldBeatRight("♡6", "♢5");
        leftShouldBeatRight("♡5", "♢4");
        leftShouldBeatRight("♡4", "♢3");
        leftShouldBeatRight("♡3", "♢2");
    }

    @Test
    @DisplayName("two cards with same value should be a tie")
    void test2() {
        leftShouldBeATieWithRight("♡A", "♢A");
        leftShouldBeATieWithRight("♡K", "♢K");
        leftShouldBeATieWithRight("♡Q", "♢Q");
        leftShouldBeATieWithRight("♡J", "♢J");
        leftShouldBeATieWithRight("♡T", "♢T");
        leftShouldBeATieWithRight("♡9", "♢9");
        leftShouldBeATieWithRight("♡8", "♢8");
        leftShouldBeATieWithRight("♡7", "♢7");
        leftShouldBeATieWithRight("♡6", "♢6");
        leftShouldBeATieWithRight("♡5", "♢5");
        leftShouldBeATieWithRight("♡4", "♢4");
        leftShouldBeATieWithRight("♡3", "♢3");
        leftShouldBeATieWithRight("♡2", "♢2");
    }

    private void leftShouldBeatRight(String left, String right) {
        assertThat(toCard(left)).isGreaterThan(toCard(right));
    }

    void leftShouldBeATieWithRight(String left, String right) {
        assertThat(toCard(left)).isEqualByComparingTo(toCard(right));
    }


}


