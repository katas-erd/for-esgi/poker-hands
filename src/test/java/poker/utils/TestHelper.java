package poker.utils;

import poker.Hand;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static poker.utils.Utils.toCards;

public class TestHelper {
    Hand actual;

    public TestHelper(Hand actual) {
        this.actual = actual;
    }

    public void should_win_against(String cards) {
        Hand expected = new Hand(toCards(cards));

        assertThat(actual).isGreaterThan(expected);

    }

    public void should_be_even(String cards) {
        Hand expected = new Hand(toCards(cards));

        assertThat(actual).isEqualByComparingTo(expected);

    }
}
