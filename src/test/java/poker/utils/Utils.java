package poker.utils;

import poker.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static poker.utils.Utils.toCards;

public class Utils {
    public static Card toCard(String s) {
        String suit = s.substring(0, 1);
        String value = s.substring(1);
        return new Card(asSuit(suit), asValue(value));
    }

    public static CardValue asValue(String s) {
        switch (s) {
            case "2": return CardValue.TWO;
            case "3": return CardValue.THREE;
            case "4": return CardValue.FOUR;
            case "5": return CardValue.FIVE;
            case "6": return CardValue.SIX;
            case "7": return CardValue.SEVEN;
            case "8": return CardValue.EIGHT;
            case "9": return CardValue.NINE;
            case "T": return CardValue.TEN;
            case "J": return CardValue.JACK;
            case "Q": return CardValue.QUEEN;
            case "K": return CardValue.KING;
            case "A": return CardValue.ACE;
            default: throw new IllegalArgumentException(s + " is not a valid value");
        }
    }

    public static CardSuit asSuit(String s) {
        switch (s) {
            case "♡": return CardSuit.HEART;
            case "♢": return CardSuit.DIAMOND;
            case "♧": return CardSuit.CLUBS;
            case "♤": return CardSuit.SPADE;
            default: throw new IllegalArgumentException(s + " is not a valid suit");
        }
    }

    public static List<Card> toCards(String representation) {
        List<Card> cards = new ArrayList<>();
        for(String card : representation.split(" ")) {
            cards.add(Utils.toCard(card));
        }
        return cards;
    }

    public static Hand hand_with(String representation) {
        return new Hand(toCards(representation));
    }

    public static OfAKind toPart(String representation) {
        List<Card> cards = toCards(representation);
        return new OfAKind(cards.get(0), cards.size());
    }

    public static List<OfAKind> toParts(String... representation) {
        List<OfAKind> ofAKinds = new ArrayList<>();
        for (String part : representation) {
            ofAKinds.add(toPart(part));
        }
        return ofAKinds;
    }

    public static TestHelper the_hand(String cards) {
        return new TestHelper(new Hand(toCards(cards)));
    }

}

